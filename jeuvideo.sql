-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 15 juin 2020 à 01:01
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `jeuvideo`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `identifiant` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  PRIMARY KEY (`identifiant`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`identifiant`, `mdp`) VALUES
('fatih', 'ceb8447cc4ab78d2ec34cd9f11e4bed2');

-- --------------------------------------------------------

--
-- Structure de la table `avisutil`
--

DROP TABLE IF EXISTS `avisutil`;
CREATE TABLE IF NOT EXISTS `avisutil` (
  `numAvis` int(11) NOT NULL AUTO_INCREMENT,
  `Commentaire` varchar(255) DEFAULT NULL,
  `pseudo` varchar(255) DEFAULT NULL,
  `numJeu` int(11) DEFAULT NULL,
  PRIMARY KEY (`numAvis`),
  KEY `fk_AvisUtil_pseudo` (`pseudo`),
  KEY `fk_AvisUtil_jeu` (`numJeu`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `comparateur`
--

DROP TABLE IF EXISTS `comparateur`;
CREATE TABLE IF NOT EXISTS `comparateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numJeu` int(11) NOT NULL,
  `numLien1` int(11) NOT NULL,
  `numLien2` int(11) NOT NULL,
  `numConsole` int(11) DEFAULT NULL,
  `imageCompa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`numJeu`,`numLien1`,`numLien2`),
  KEY `fk_comparateur_numJeu` (`numJeu`),
  KEY `fk_comparateur_numLien1` (`numLien1`),
  KEY `fk_comparateur_numLien2` (`numLien2`),
  KEY `fk_comparateur_numconsole` (`numConsole`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `comparateur`
--

INSERT INTO `comparateur` (`id`, `numJeu`, `numLien1`, `numLien2`, `numConsole`, `imageCompa`) VALUES
(1, 1, 1, 2, 2, '/Image/XBOX/BO4.jpg'),
(2, 1, 3, 4, 1, '/Image/PS4/BO4.jpg'),
(3, 1, 5, 6, 4, '/Image/PC/BO4.jpg'),
(4, 2, 7, 8, 2, '/Image/XBOX/dbz.jpg'),
(5, 2, 9, 10, 1, '/Image/PS4/dbz.jpg'),
(6, 2, 11, 12, 3, '/Image/Switch/dbz.jpg'),
(7, 3, 13, 14, 1, '/Image/PS4/fifa.jpg'),
(8, 3, 15, 16, 4, '/Image/PC/fifa.jpg'),
(9, 3, 17, 18, 2, '/Image/XBOX/fifa.jpg'),
(10, 4, 19, 20, 1, '/Image/PS4/GTA.jpg'),
(11, 4, 21, 22, 2, '/Image/XBOX/GTA.jpg'),
(12, 1, 1, 2, 2, '/Image/XBOX/BO4.jpg'),
(13, 1, 3, 4, 1, '/Image/PS4/BO4.jpg'),
(14, 1, 5, 6, 4, '/Image/PC/BO4.jpg'),
(15, 2, 7, 8, 2, '/Image/XBOX/dbz.jpg'),
(16, 2, 9, 10, 1, '/Image/PS4/dbz.jpg'),
(17, 2, 11, 12, 3, '/Image/Switch/dbz.jpg'),
(18, 3, 13, 14, 1, '/Image/PS4/fifa.jpg'),
(19, 3, 15, 16, 4, '/Image/PC/fifa.jpg'),
(20, 3, 17, 18, 2, '/Image/XBOX/fifa.jpg'),
(21, 4, 19, 20, 1, '/Image/PS4/GTA.jpg'),
(22, 4, 21, 22, 2, '/Image/XBOX/GTA.jpg'),
(23, 4, 23, 23, 4, '/Image/PC/GTA.jpg'),
(24, 5, 24, 25, 1, '/Image/PS4/RDR2.jpg'),
(25, 5, 26, 27, 2, '/Image/XBOX/RDR2.jpg'),
(26, 6, 28, 29, 1, '/Image/PS4/nfs.jpg'),
(27, 6, 30, 31, 2, '/Image/XBOX/nfs.jpg'),
(28, 6, 32, 33, 4, '/Image/PC/nfs.jpg'),
(29, 8, 34, 35, 3, '/Image/Switch/mario.jpg'),
(30, 9, 36, 37, 3, '/Image/Switch/pokemon.jpg'),
(31, 10, 38, 39, 3, '/Image/Switch/smash.jpg'),
(32, 11, 40, 41, 2, '/Image/XBOX/forza.jpg'),
(33, 12, 42, 43, 1, '/Image/PS4/division.jpg'),
(34, 12, 44, 45, 2, '/Image/XBOX/division.jpg'),
(35, 12, 46, 47, 4, '/Image/PC/division.jpg'),
(36, 13, 48, 49, 1, '/Image/PS4/far.jpg'),
(37, 13, 50, 51, 2, '/Image/XBOX/far.jpg'),
(38, 14, 52, 53, 1, '/Image/PS4/jump.jpg'),
(39, 14, 54, 55, 2, '/Image/XBOX/jump.jpg'),
(40, 14, 56, 56, 4, '/Image/PC/jump.jpg'),
(41, 15, 57, 58, 1, '/Image/PS4/devil.jpg'),
(42, 15, 59, 60, 2, '/Image/XBOX/devil.jpg'),
(43, 15, 61, 62, 4, '/Image/PC/devil.jpg'),
(44, 16, 63, 64, 1, '/Image/PS4/metro.jpg'),
(45, 16, 65, 66, 2, '/Image/XBOX/metro.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `console`
--

DROP TABLE IF EXISTS `console`;
CREATE TABLE IF NOT EXISTS `console` (
  `numConsole` int(11) NOT NULL AUTO_INCREMENT,
  `nomConsole` varchar(255) DEFAULT NULL,
  `marque` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`numConsole`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `console`
--

INSERT INTO `console` (`numConsole`, `nomConsole`, `marque`, `image`) VALUES
(1, 'PS4', 'SONY', '/Image/console/PS4.jpg'),
(2, 'XBOX ONE', 'MICROSOFT', '/Image/console/XBOX.jpg'),
(3, 'Switch', 'Nintendo', '/Image/console/switch.jpg'),
(4, 'PC', NULL, '/Image/console/PC.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `inscription`
--

DROP TABLE IF EXISTS `inscription`;
CREATE TABLE IF NOT EXISTS `inscription` (
  `pseudo` varchar(255) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `mdp` varchar(255) NOT NULL,
  `dateNaissance` date DEFAULT NULL,
  PRIMARY KEY (`pseudo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `inscription`
--

INSERT INTO `inscription` (`pseudo`, `nom`, `prenom`, `mdp`, `dateNaissance`) VALUES
('RollandLieur', '  Mai  ', 'Roland', '209b9772e848c234bc33c8c65456eda5', '1999-02-11'),
('fatih94230', 'Altintas', 'Fatih', 'b4b643cb1547b52057fdd15336581ca8', '2000-01-01');

-- --------------------------------------------------------

--
-- Structure de la table `jeux`
--

DROP TABLE IF EXISTS `jeux`;
CREATE TABLE IF NOT EXISTS `jeux` (
  `numJeu` int(20) NOT NULL AUTO_INCREMENT,
  `nomJeu` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `imageJeu` varchar(255) NOT NULL,
  PRIMARY KEY (`numJeu`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `jeux`
--

INSERT INTO `jeux` (`numJeu`, `nomJeu`, `age`, `description`, `imageJeu`) VALUES
(1, 'Call Of Duty Black Ops 4 ', '/Image/age/18.jpg', 'Jeu de guerre', 'Image/Bo4.jpg'),
(2, 'Dragon Ball FighterZ', '/Image/age/12.jpg', 'Jeu de combat', 'Image/fighterZ.jpg'),
(3, 'Fifa 19', '/Image/age/3.jpg', 'Jeu de foot', 'Image/fifa.jpg'),
(4, 'Grand Theft Auto V', '/Image/age/18.jpg', 'Jeu d\'action', 'Image/GTA.jpg'),
(5, 'Read Dead Redemption 2', '/Image/age/18.jpg', 'Jeu d\'action', 'Image/Redead.jpg'),
(6, 'Need For Speed payback', '/Image/age/12.jpg', 'Jeu de course', 'Image/nfs.jpg'),
(8, 'Mario Kart 8', '/Image/age/3.jpg', 'Jeu de course', 'Image/marioKart.jpg'),
(9, 'Pokemon Pikachu', '/Image/age/7.jpg', 'Jeu RPG', 'Image/pokemon.png'),
(10, 'Super Smash Bros Ultimate', '/Image/age/12.jpg', 'Jeu de combat', 'Image/smash.jpg'),
(11, 'Forza Horizon 4', '/Image/age/3.jpg', 'Jeu de course', 'Image/forza.jpg'),
(12, 'The Division 2', '/Image/age/18.jpg', 'Jeu de guerre', 'Image/division.jpg'),
(13, 'Far Cry 5', '/Image/age/18.jpg', 'Jeu d\'action', 'Image/farcry.jpg'),
(14, 'Jump Force', '/Image/age/12.jpg', 'Jeu de Combat', 'Image/jump.jpg'),
(15, 'Devil May Cry 5', '/Image/age/18.jpg', 'Jeu d\'action', 'Image/devil.jpg'),
(16, 'Metro Exodus', '/Image/age/18.jpg', 'Jeu de guerre', 'Image/metro.jpg'),
(18, 'Days Gone', '/Image/age/18.jpg', 'Jeu d\'aventure', 'Image/daysGone.jpg'),
(19, 'Sekiro Shadows Die Twice', '/Image/age/18.jpg', 'Jeu d\'aventure', 'Image/sekiro.jpg'),
(20, 'Mortal Kombat 11', '/Image/age/18.jpg', 'Jeu de combat', 'Image/mortal.jpg'),
(22, 'Apex Legends', '/Image/age/12.jpg', 'Jeu de tir', 'Image/apex.jpg'),
(23, 'World War z', '/Image/age/18.jpg', 'Jeu de tir', 'Image/wwz.jpg'),
(24, 'Anthem', '/Image/age/16.jpg', 'Jeu d\'action', 'Image/anthem.jpg'),
(25, 'Assassin\'s Creed Odyssey', '/Image/age/18.jpg', 'Jeu d\'aventure', 'Image/ac.jpg'),
(26, 'Borderlands 3', '/Image/age/18.jpg', 'Jeu d\'action', 'Image/borderland3.jpg'),
(27, 'Star Wars Jedi : Fallen Order', '/Image/age/18.jpg', 'Jeu d\'aventure', 'Image/starwars.jpg'),
(28, 'Pokémon Epée / Bouclier', '/Image/age/3.jpg', 'Jeu d\'aventure', 'Image/pokemon.jpg'),
(29, 'RAGE 2', '/Image/age/18.jpg', 'Jeu de tir', 'Image/rage2.jpg'),
(30, 'Death Stranding', '/Image/age/18.jpg', 'Jeu d\'aventure', 'Image/deathstranding.jpg'),
(31, 'Crash Team Racing Nitro-Fueled', '/Image/age/3.jpg', 'Jeu de course', 'Image/crash.jpg'),
(32, 'Judgment', '/Image/age/18.jpg', 'Jeu d\'action', 'Image/judgment.jpg'),
(33, 'The Outer Worlds', '/Image/age/16.jpg', 'Jeu d\'action', 'Image/outerworlds.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `jeuxattendu`
--

DROP TABLE IF EXISTS `jeuxattendu`;
CREATE TABLE IF NOT EXISTS `jeuxattendu` (
  `numJeu` int(11) NOT NULL,
  PRIMARY KEY (`numJeu`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `jeuxattendu`
--

INSERT INTO `jeuxattendu` (`numJeu`) VALUES
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33);

-- --------------------------------------------------------

--
-- Structure de la table `jeuxpopu`
--

DROP TABLE IF EXISTS `jeuxpopu`;
CREATE TABLE IF NOT EXISTS `jeuxpopu` (
  `numJeu` int(11) NOT NULL,
  PRIMARY KEY (`numJeu`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `jeuxpopu`
--

INSERT INTO `jeuxpopu` (`numJeu`) VALUES
(12),
(18),
(19),
(20),
(22),
(23),
(24),
(25);

-- --------------------------------------------------------

--
-- Structure de la table `sitevente`
--

DROP TABLE IF EXISTS `sitevente`;
CREATE TABLE IF NOT EXISTS `sitevente` (
  `numLien` int(11) NOT NULL AUTO_INCREMENT,
  `lien` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`numLien`)
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `sitevente`
--

INSERT INTO `sitevente` (`numLien`, `lien`, `description`) VALUES
(1, 'https://www.amazon.fr/Call-Duty-Black-Ops-4/dp/B07BC5ZH7S/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=black+ops+4&qid=1556834258&s=videogames&sr=1-3', 'Call Of Duty Black Ops 4 amazon XBOX'),
(2, 'https://jeux-video.fnac.com/a12545755/Call-of-Duty-Black-Ops-4-Xbox-One-Jeu-Xbox-One?omnsearchpos=2', 'Call Of Duty Black Ops 4 fnac XBOX'),
(3, 'https://www.amazon.fr/Call-Duty-Black-Ops-4/dp/B07BCCVGRB/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=bo4&qid=1556834533&s=gateway&sr=8-1', 'Call Of Duty Black Ops 4 amazon PS4'),
(4, 'https://jeux-video.fnac.com/a12545753/Call-of-Duty-Black-Ops-4-PS4-Jeu-PlayStation-4?omnsearchpos=1', 'Call Of Duty Black Ops 4 fnac PS4'),
(5, 'https://www.amazon.fr/Call-Duty-Black-Ops-4/dp/B07BBYQR97/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=black+ops+4+pc&qid=1556834812&s=gateway&sr=8-1', 'Call Of Duty Black Ops 4 amazon PC'),
(6, 'https://jeux-video.fnac.com/a12545754/Call-of-Duty-Black-Ops-4-PC-CD-ROM-PC?omnsearchpos=3', 'Call Of Duty Black Ops 4 fnac PC'),
(7, 'https://www.amazon.fr/Dragon-Ball-FighterZ-Xbox-One/dp/B073WY3PX4/ref=sr_1_2?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2OGDI3JYGR6LR&keywords=dbz+fighter+z&qid=1556835025&s=gateway&sprefix=dbz+fight%2Caps%2C128&sr=8-2', 'Dragon Ball FighterZ amazon XBOX'),
(8, 'https://jeux-video.fnac.com/a10949222/Dragon-Ball-Fighter-Z-Xbox-One-Jeu-Xbox-One?omnsearchpos=19', 'Dragon Ball FighterZ fnac XBOX'),
(9, 'https://www.amazon.fr/Bandai-Namco-Entertainment-3391891995405-DragonBall/dp/B073183DCG/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2OGDI3JYGR6LR&keywords=dbz+fighter+z&qid=1556835025&s=gateway&sprefix=dbz+fight%2Caps%2C128&sr=8-1', 'Dragon Ball FighterZ amazon PS4'),
(10, 'https://jeux-video.fnac.com/a10949221/Dragon-Ball-Fighter-Z-PS4-Jeu-PlayStation-4?omnsearchpos=3', 'Dragon Ball FighterZ fnac PS4'),
(11, 'https://jeux-video.fnac.com/a12518578/Dragon-Ball-Fighter-Z-Nintendo-Switch-Jeu-Nintendo-Switch?omnsearchpos=1', 'Dragon Ball FighterZ fnac Switch'),
(12, 'https://www.amazon.fr/Bandai-Namco-Entertainment-3391891998932-Fighter/dp/B07DP1JB6J/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=N0QNCXOW5VCN&keywords=dbz+fighter+z+switch&qid=1556835305&s=gateway&sprefix=dbz+fighter+z+sw%2Caps%2C126&sr=8-1', 'Dragon Ball FighterZ amazon Switch'),
(13, 'https://www.amazon.fr/Electronic-Arts-FIFA-19/dp/B07DNXDH5N/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=fifa+19&qid=1556835391&s=gateway&sr=8-1', 'Fifa 19 amazon PS4'),
(14, ' http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/FIFA-19-PS4/a12513980/w-4?oref=b2eec162-08b0-97c9-fa20-a0f4038f835e&Origin=PA_JV_BOUTIQUE', 'Fifa 19 fnac PS4'),
(15, 'https://www.amazon.fr/dp/B07DP2CYG2/ref=asc_df_B07DP2CYG21556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07DP2CYG2&linkCode=df0', 'Fifa 19 amazon PC'),
(16, ' http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/FIFA-19-PC/a12513983/w-4?oref=52a07941-58d3-1878-c494-58f7b7f778b7&Origin=PA_JV_BOUTIQUE', 'Fifa 19 fnac PC'),
(17, 'https://www.amazon.fr/dp/B07DNN4PLH/ref=asc_df_B07DNN4PLH1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07DNN4PLH&linkCode=df0', 'Fifa 19 amazon XBOX'),
(18, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/FIFA-19-Xbox-One/a12513981/w-4?oref=420a0ada-4b60-2b25-a715-c005d7d56e47&Origin=PA_JV_BOUTIQUE', 'Fifa 19 fnac XBOX'),
(19, 'https://www.amazon.fr/Rockstar-Games-GTA-V/dp/B076FC1JYM/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2DZQTQ1FW12ES&keywords=gta+5+ps4&qid=1556835988&s=gateway&sprefix=gta%2Caps%2C135&sr=8-1', 'Grand Theft Auto V amazon PS4'),
(20, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/GTA-5-PS4/a11139273/w-4?oref=6d398bcd-b785-bc25-b49a-c100b16d6058&Origin=PA_JV_BOUTIQUE', 'Grand Theft Auto V fnac PS4'),
(21, 'https://www.amazon.fr/dp/B00KWBFXLI/ref=asc_df_B00KWBFXLI1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B00KWBFXLI&linkCode=df0', 'Grand Theft Auto V amazon XBOX'),
(22, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/GTA-5-Xbox-One/a11139274/w-4?oref=404a41e0-cd04-9aa4-101d-a37158046872&Origin=PA_JV_BOUTIQUE', 'Grand Theft Auto V fnac XBOX'),
(23, 'https://www.amazon.fr/dp/B00KWBG038/ref=asc_df_B00KWBG0381556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B00KWBG038&linkCode=df0', 'Grand Theft Auto V amazon PC'),
(24, 'https://www.amazon.fr/dp/B01M7RRZ8D/ref=asc_df_B01M7RRZ8D1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B01M7RRZ8D&linkCode=df0', 'Read Dead Redemption 2 amazon PS4'),
(25, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Red-Dead-Redemption-2-PS4/a10171259/w-4?oref=14e9778a-3785-13b3-1c71-b588cad4a456&Origin=PA_JV_BOUTIQUE', 'Read Dead Redemption 2 fnac PS4'),
(26, 'https://www.amazon.fr/dp/B01MAYQD6B/ref=asc_df_B01MAYQD6B1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B01MAYQD6B&linkCode=df0', 'Read Dead Redemption 2 amazon XBOX'),
(27, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Red-Dead-Redemption-2-Xbox-One/a10171260/w-4?oref=5563f131-31de-85b1-6b83-09f6d7da9b45&Origin=PA_JV_BOUTIQUE', 'Read Dead Redemption 2 fnac XBOX'),
(28, 'https://www.amazon.fr/dp/B071JF3CC1/ref=asc_df_B071JF3CC11556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B071JF3CC1&linkCode=df0', 'Need for speed payback amazon PS4'),
(29, 'https://jeux-video.fnac.com/a10716010/Need-For-Speed-PayBack-PS4-Jeu-PlayStation-4?omnsearchpos=', 'Need for speed payback fnac PS4'),
(30, 'https://www.amazon.fr/dp/B071WC822B/ref=asc_df_B071WC822B1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B071WC822B&linkCode=df0', 'Need for speed payback amazon XBOX'),
(31, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Need-For-Speed-PayBack-Xbox-One/a10716011/w-4?oref=05d3f950-d874-f7f9-5319-c50b3392d7d2&Origin=PA_JV_BOUTIQUE', 'Need for speed payback fnac XBOX'),
(32, 'https://www.amazon.fr/dp/B071ZS9SXN/ref=asc_df_B071ZS9SXN1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B071ZS9SXN&linkCode=df0', 'Need for speed payback amazon PC'),
(33, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Need-For-Speed-PayBack-PC/a10716012/w-4?oref=00000000-0000-0000-0000-000000000000&Origin=PA_JV_BOUTIQUE', 'Need for speed payback fnac PC'),
(34, 'https://www.amazon.fr/dp/B01N223WHL/ref=asc_df_B01N223WHL1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B01N223WHL&linkCode=df0', 'Mario Kart 8 amazon Switch'),
(35, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Mario-Kart-8-Deluxe-Nintendo-Switch/a10337751/w-4?oref=d637198e-d938-ebb7-e032-30937e7c2d29&Origin=PA_JV_BOUTIQUE', 'Mario Kart 8 fnac Switch'),
(36, 'https://www.amazon.fr/dp/B07DF4HGBY/ref=asc_df_B07DF4HGBY1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07DF4HGBY&linkCode=df0', 'Pokemon Pikachu amazon Switch'),
(37, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Pokemon-Let-s-Go-Pikachu-Nintendo-Switch/a12876492/w-4?oref=00000000-0000-0000-0000-000000000000&Origin=PA_JV_BOUTIQUE', 'Pokemon Pikachu fnac Switch'),
(38, 'https://www.amazon.fr/dp/B07DPQD26J/ref=asc_df_B07DPQD26J1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07DPQD26J&linkCode=df0', 'Super Smash Bros Ultimate amazon Switch'),
(39, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Super-Smash-Bros-Ultimate-Nintendo-Switch/a12979269/w-4?oref=00000000-0000-0000-0000-000000000000&Origin=PA_JV_BOUTIQUE', 'Super Smash Bros Ultimate fnac Switch'),
(40, 'https://www.amazon.fr/dp/B07DNQX5H7/ref=asc_df_B07DNQX5H71556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07DNQX5H7&linkCode=df0', 'Forza Horizon 4 amazon XBOX'),
(41, 'https://jeux-video.fnac.com/a12817551/Forza-Horizon-4-Xbox-One-Jeu-Xbox-One?omnsearchpos=3', 'Forza Horizon 4 fnac XBOX'),
(42, 'https://www.amazon.fr/dp/B07GJ1GKYC/ref=asc_df_B07GJ1GKYC1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07GJ1GKYC&linkCode=df0', 'The Division 2 amazon PS4'),
(43, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/The-Division-2-PS4/a12508092/w-4?oref=fb64725d-6c08-313e-a569-40227722acd1&Origin=PA_JV_BOUTIQUE', 'The Division 2 fnac PS4'),
(44, 'https://www.amazon.fr/dp/B07GJ1GKYD/ref=asc_df_B07GJ1GKYD1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07GJ1GKYD&linkCode=df0', 'The Division 2 amazon XBOX'),
(45, 'https://jeux-video.fnac.com/a12508093/The-Division-2-Xbox-One-Jeu-Xbox-One?omnsearchpos=2', 'The Division 2 fnac XBOX'),
(46, 'https://www.amazon.fr/dp/B07NJRYGMF/ref=asc_df_B07NJRYGMF1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07NJRYGMF&linkCode=df0', 'The Division 2 amazon PC'),
(47, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Tom-Clancy-s-The-Division-2-PC/a13299050/w-4?oref=6c807e1f-c6c0-9eb4-7592-a2709a78e699&Origin=PA_JV_BOUTIQUE', 'The Division 2 fnac PC'),
(48, 'https://www.amazon.fr/dp/B072PQWS69/ref=asc_df_B072PQWS691556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B072PQWS69&linkCode=df0', 'Far Cry 5 amazon PS4'),
(49, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Far-Cry-5-PS4/a10700709/w-4?oref=69a84c3d-3c86-a02e-c8d4-da5331284b25&Origin=PA_JV_BOUTIQUE', 'Far Cry 5 fnac PS4'),
(50, 'https://www.amazon.fr/dp/B0784YKRVL/ref=asc_df_B0784YKRVL1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B0784YKRVL&linkCode=df0', 'Far Cry 5 amazon XBOX'),
(51, 'https://jeux-video.fnac.com/a10700710/Far-Cry-5-Xbox-One-Jeu-Xbox-One?omnsearchpos=5', 'Far Cry 5 fnac XBOX'),
(52, 'https://www.amazon.fr/dp/B07DQ76NTS/ref=asc_df_B07DQ76NTS1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07DQ76NTS&linkCode=df0', 'Jump Force amazon PS4'),
(53, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Jump-Force-PS4/a12988532/w-4?oref=00000000-0000-0000-0000-000000000000&Origin=PA_JV_BOUTIQUE', 'Jump Force fnac PS4'),
(54, 'https://www.amazon.fr/dp/B07DQ1HTGQ/ref=asc_df_B07DQ1HTGQ1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07DQ1HTGQ&linkCode=df0', 'Jump Force amazon XBOX'),
(55, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Jump-Force-Xbox-One/a12988533/w-4?oref=00000000-0000-0000-0000-000000000000&Origin=PA_JV_BOUTIQUE', 'Jump Force fnac XBOX'),
(56, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Jump-Force/a13341969/w-4?oref=00000000-0000-0000-0000-000000000000&Origin=PA_JV_BOUTIQUE', 'Jump Force fnac PC'),
(57, 'https://www.amazon.fr/dp/B07FD3W69R/ref=asc_df_B07FD3W69R1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07FD3W69R&linkCode=df0', 'Devil May Cry 5 amazon PS4'),
(58, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Devil-May-Cry-5-PS4/a13170843/w-4?oref=632c62ce-f17d-665e-2e12-35d0794015a5&Origin=PA_JV_BOUTIQUE', 'Devil May Cry 5 fnac PS4'),
(59, 'https://www.amazon.fr/dp/B07FCSLDNX/ref=asc_df_B07FCSLDNX1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07FCSLDNX&linkCode=df0', 'Devil May Cry 5 amazon XBOX'),
(60, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Devil-May-Cry-5-Xbox-One/a13170845/w-4?oref=81c33b87-f0f3-268b-2c49-31d108d5f28a&Origin=PA_JV_BOUTIQUE', 'Devil May Cry 5 fnac XBOX'),
(61, 'https://www.amazon.fr/dp/B07GQVTVDP/ref=asc_df_B07GQVTVDP1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B07GQVTVDP&linkCode=df0', 'Devil May Cry 5 amazon PC'),
(62, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Devil-May-Cry-5-PC/a13170844/w-4?oref=00000000-0000-0000-0000-000000000000&Origin=PA_JV_BOUTIQUE', 'Devil May Cry 5 fnac PC'),
(63, ' https://www.amazon.fr/dp/B072VJZ6HG/ref=asc_df_B072VJZ6HG1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B072VJZ6HG&linkCode=df0 ', 'Metro Exodus amazon PS4'),
(64, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Metro-Exodus-PS4/a10780519/w-4?oref=9e856d1c-51ff-a700-a75a-69a42fac52b2&Origin=PA_JV_BOUTIQUE', 'Metro Exodus fnac PS4'),
(65, 'https://www.amazon.fr/dp/B072VKG8FB/ref=asc_df_B072VKG8FB1556708400000/?tag=jeuxvideocom-21&creative=22950&creativeASIN=B072VKG8FB&linkCode=df0', 'Metro Exodus amazon XBOX'),
(66, 'http://eultech.fnac.com/dynclick/fnac/?ept-publisher=JEUXVIDEO&ept-name=JEUXVIDEO&eurl=https://www.fnac.com/Metro-Exodus-Xbox-One/a10780526/w-4?oref=8c5b0312-9a22-a6ff-cfce-a75d8b7cf891&Origin=PA_JV_BOUTIQUE', 'Metro Exodus fnac XBOX');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
