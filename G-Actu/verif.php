<?php
session_start();
$identifiant=$_POST["identi"];
$mdp=md5($_POST["mdp"]);

require "connexionBDD.php";
$bdd=connect();
$sql= $bdd->prepare("Select * from admin where identifiant=:identifiant and mdp =:mdp") ;
$sql -> bindParam('identifiant',$identifiant,PDO::PARAM_STR) ;
$sql -> bindParam('mdp',$mdp,PDO::PARAM_STR) ;
$sql-> execute();
$rep = $sql->fetch(PDO::FETCH_BOTH);
$nb_lignes = $sql->rowCount();

//$sql-> execute(array ‘login’ => ‘$login’, ‘mdp’ => ‘$mdp’)) ;

if($nb_lignes==0){
	echo "ERREUR DE CONNEXION";
	?>
	<a href="loginAdmin.php"> Reessayer de vous connecter</a>
	<?php
}
else{
	$_SESSION["admin"] = $rep -> identifiant;
	$_SESSION["autorisationAdmin"]="OK";
	header("location:Admin.php");
}
?>