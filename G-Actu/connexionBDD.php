<?php
require "config.php";
function connect() // fonction de connexion
{
try // Essaye de ......
{
	//connexion à la BDD
	$connect = new PDO ('mysql:host='.HOTE.';dbname='.BDD,UTILISATEUR,MDP,array(PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES UTF8'));
	$connect->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}
catch (PDOExeption $e) // Attrape l'erreur si cela ne marche pas...
{
	echo "Problème de connexion à la BDD <br>". $e-> getMessage();
	
}
return $connect;
}

?>