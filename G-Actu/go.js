window.onload = function()
{
	var canvasLargeur=900;
	var canvasHauteur=600 ;
	var tailleBloc=30 ;
	var ctx;
	var delai=100 ;
	var snakee;
	var apple;
	var largeurNbBlocs= canvasLargeur/tailleBloc ;
	var hauteurNbBlocs= canvasHauteur/tailleBloc ;
	var score ;
	
	init() ;
	
	function init()
	{
	var canvas = document.createElement('canvas') ;
	canvas.width = canvasLargeur;
	canvas.height = canvasHauteur;
	canvas.style.border="30px solid gray" ;
	canvas.style.margin="50px auto";
	canvas.style.display="block";
	canvas.style.backgroundColor="#ddd";
	document.body.appendChild(canvas) ;
	ctx=canvas.getContext('2d') ;
	
	snakee=new serpent([[6,4],[5,4],[4,4], [3,4], [2,4]],"droite") ;
	
	apple=new pomme([10,10]) ;
	score=0;
	rafraichirCanvas() ;
	
	} ;
	
	function rafraichirCanvas()
	{
	snakee.avancer() ;
	if(snakee.verifCollision())
		{
		gameOver() ;
		}
		else
		{
		if(snakee.mangePomme(apple))
			{
				score++;
				snakee.pomme_mangee=true ;
				do
				{
				apple.replacerPomme() ;
				} while(apple.est_sur_serpent(snakee)) ;
			}
		ctx.clearRect(0,0,canvasLargeur, canvasHauteur);
		afficherScore() ;
		snakee.dessiner() ;
		apple.dessiner() ;
		setTimeout(rafraichirCanvas, delai);
		}
	};
	
	function gameOver()
	{
		ctx.save() ;
		ctx.font = "bold 70px sans-serif";
        ctx.fillStyle = "black";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.strokeStyle = "white";
        ctx.lineWidth = 5;
        var centreX = canvasLargeur / 2;
        var centreY = canvasHauteur / 2;
        ctx.strokeText("Game Over", centreX, centreY - 180);
        ctx.fillText("Game Over", centreX, centreY - 180);
        ctx.font = "bold 30px sans-serif";
        ctx.strokeText("Appuyer sur la touche Espace pour rejouer", centreX, centreY - 120);
        ctx.fillText("Appuyer sur la touche Espace pour rejouer", centreX, centreY - 120);
		ctx.restore() ;
	};
	
	function restart() 
	{
		snakee=new serpent([[6,4],[5,4],[4,4], [3,4], [2,4]],"droite") ;
		apple=new pomme([10,10]) ;
		rafraichirCanvas() ;
		score=0;
		
	}
	
	function afficherScore()
	{
		ctx.save() ;
		ctx.font = "bold 200px sans-serif";
		ctx.fillStyle = "gray" ;
		ctx.textAlign = "center";
		ctx.textBaseline="middle" ;
		var centreX=canvasLargeur/2;
		var centreY=canvasHauteur/2;
		ctx.fillText(score.toString(), centreX,centreY) ;
		ctx.restore() ; ;
	}
	
	function dessinerBloc(ctx, position)
	{
		var x=position[0]*tailleBloc ;
		var y=position[1]*tailleBloc ;
		ctx.fillRect(x,y, tailleBloc, tailleBloc);
	};
	
	function serpent(corps,direction)
	{
		this.corps=corps ;
		this.direction=direction ;
		this.pomme_mangee =false;
		this.dessiner=function()
			{
			ctx.save() ;
			ctx.fillStyle = "#ff0000" ;
			for(var i=0; i< this.corps.length;i++) 
			{
				dessinerBloc(ctx, this.corps[i]);
			}
			ctx.restore() ;
			
			}	;
	
		this.avancer=function()
			{
				var nouvellePosition=this.corps[0].slice() ;
				switch(this.direction){
                case "gauche":
                    nouvellePosition[0] -= 1;
                    break;
                case "droite":
                    nouvellePosition[0] += 1;
                    break;
                case "bas":
                    nouvellePosition[1] += 1;
                    break;
                case "haut":
                    nouvellePosition[1] -= 1;
                    break;
                default:
                    throw("invalid direction");
										}
				this.corps.unshift(nouvellePosition);
				if(this.pomme_mangee===false)
					{
					this.corps.pop();
					}
					else
					{
					this.pomme_mangee=false ;
					}
			
			} ;
			
			this.setDirection = function(nouvelleDirection){
            var directionPermise;
            switch(this.direction){
                case "gauche":
					directionPermise=["haut","bas"];
                    break;
                case "droite":
                    directionPermise=["haut","bas"];
                    break;
                case "bas":
					directionPermise=["gauche","droite"];
                    break;
                case "haut":
                    directionPermise=["gauche","droite"];
                    break;  
               default:
                    throw("direction invalide");
            }
            if (directionPermise.indexOf(nouvelleDirection) > -1)
			{
                this.direction = nouvelleDirection;
            }
			} ;
			
			this.verifCollision = function()
			{
			var collisionMur = false ;
			var collisionCorps = false ;
			var tete=this.corps[0];
			var reste=this.corps.slice(1) ;
			var Xtete= tete[0];
			var Ytete=tete[1];
			var minX=0;
			var minY=0;
			var maxX=largeurNbBlocs-1;
			var maxY=hauteurNbBlocs-1;
			var sortMursHorizontaux = Xtete <minX || Xtete >maxX ;
			var sortMursVerticaux = Ytete<minY || Ytete > maxY ;
			
			if(sortMursHorizontaux || sortMursVerticaux )
				{
				collisionMur= true ;
				}
			for(var i=0; i<reste.length;i++)
				{
				if(Xtete===reste[i][0] && Ytete=== reste[i][1])
					{
					collisionCorps=true ;
					}
				}
			
			return collisionMur || collisionCorps;
			} ;
			
			this.mangePomme= function(pomme_a_manger)

				{
				var tete= this.corps[0];
				if(tete[0]=== pomme_a_manger.position[0] && tete[1]===pomme_a_manger.position[1])
					{
					return true ;
					}
					else
					{
					return false ;
					}
				};
			
        
	} ;
	
	function pomme(position)
	{
		this.position=position ;
		this.dessiner =function()
			{
			ctx.save() ;
			ctx.fillStyle = "#33cc33";
			ctx.beginPath();
			var rayon = tailleBloc/2;
			var x = this.position[0]*tailleBloc + rayon;
			var y = this.position[1]*tailleBloc + rayon;
			ctx.arc(x, y, rayon, 0, Math.PI*2, true);
			ctx.fill();
			ctx.restore();
			} ;
		this.replacerPomme = function()
			{
			var nouveauX = Math.round(Math.random()*(largeurNbBlocs-1));
            var nouveauY = Math.round(Math.random()*(hauteurNbBlocs-1));
            this.position = [nouveauX,nouveauY];
			};
			
		this.est_sur_serpent =function(serpent_a_verifier)
			{
			var est_sur_serpent=false ;
			for(var i=0; i<serpent_a_verifier.corps.length ; i++)
				{
				if(this.position[0]===serpent_a_verifier.corps[i][0] && this.position[1]===serpent_a_verifier.corps[i][1] )
					{
					est_sur_serpent=true ;
					}
				return est_sur_serpent ;
				}
			}
		
	} ;
	
	document.onkeydown = function gererClavier(e)
	{
        var touche = e.keyCode;
        var nouvelleDirection;
        switch(touche)
		{
            case 37:
                nouvelleDirection = "gauche";
                break;
            case 38:
                nouvelleDirection = "haut";
                break;
            case 39:
                nouvelleDirection = "droite";
                break;
            case 40:
                nouvelleDirection = "bas";
                break;
			case 32:
				restart() ;
				return ;
			
            default:
                return;
        }
		snakee.setDirection(nouvelleDirection);
	} ;
}