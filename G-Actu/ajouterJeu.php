
			<?php 
			session_start();
			  if(isset($_SESSION["autorisationAdmin"]))
			  {

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Gaming actu</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <!-- Fancybox-->
    <link rel="stylesheet" href="vendor/@fancyapps/fancybox/jquery.fancybox.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <header class="header">
      <!-- Main Navbar-->
      <nav class="navbar navbar-expand-lg fixed-top">
        <div class="search-area">
          <div class="search-area-inner d-flex align-items-center justify-content-center">
            <div class="close-btn"><i class="icon-close"></i></div>
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                <form action="#">
                  <div class="form-group">
                    <input type="search" name="search" id="search" placeholder="Qu'est ce que vous voulez rechercher ?" >
                    <button type="submit" class="submit"><i class="icon-search-1"></i></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          
          <div class="navbar-header d-flex align-items-center justify-content-between">
            <a href="index.php" class="navbar-brand"><img src="Image/logo.png" style="height:150px;"></a>
            
            <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span></span><span></span><span></span></button>
          </div>
          
          <div id="navbarcollapse" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a href="index.php" class="nav-link active ">Accueil</a>
              </li>
              <li class="nav-item"><a href="actualite.php" class="nav-link ">Actualité</a>
              </li>
              <li class="nav-item"><a href="comparateur.php" class="nav-link ">Comparateur</a>
              </li>
			  <li class="nav-item"><a href="Jeux.php" class="nav-link ">Jeux</a>
              </li>
			  <li class="nav-item"><a href="Snake.php" class="nav-link ">Snake</a>
              </li>
			<li class="nav-item"><a href="admin.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexionAdmin.php" class="nav-link ">Se déconnecter</a>
              </li>
            </ul>
            <div class="navbar-text">
			
			<form class="form-inline my-2 my-lg-0" action="recherche.php" method="POST">
				<input class="form-control mr-sm-2" type="search" name="produit" placeholder="Produit" aria-label="Produit">
				<button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="icon-search-1"></i> </button>
			</form>
            </div>
            
          </div>
        </div>
      </nav>
    </header>
    
    <section style="margin-top:10%;background: url(Image/game.jpg); background-size: cover; background-position: center center" class="hero">
      <div class="container">
		</div>	  
    </section>
		<br>

		<div class="form-group" style="margin-left:5%;margin-right:5%;">
		<form action="AjouterJeu2.php" method="POST"> 
      <input type="text" class="form-control" placeholder="Nom du jeu" name="nomJeu">
	</div>
    <br> </br>
		<div class="form-group" style="margin-left:5%;margin-right:5%;">
      <input type="text" class="form-control" placeholder="Description" name="Description">
	  </div>
		<div class="form-group" style="margin-left:5%;margin-right:5%;">
  <input type="file" name='photoAge'>
  </div>
		<div class="form-group" style="margin-left:5%;margin-right:5%;">
  <input type="file" name='photoJeu'>
  </div>
<br> </br>
 <button type="submit" class="btn btn-danger" style="margin-left:5%;margin-right:5%;">Ajouter</button>
 
</form>
<footer class="main-footer" style="margin-top:5%;">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo">
              <h6 class="text-white">G-Actu</h6>
            </div>
            <div class="contact-details">
              
              <p>Email: <a href="mailto:info@company.com">fatih-altintas.pro@outlook.fr</a></p>
			  <p>Email: <a href="mailto:info@company.com">nassim.bougtib@hotmail.com</a></p>
			  <p>Email: <a href="mailto:info@company.com">firask_92@hotmail.fr</a></p>
              <ul class="social-menu">
                <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
               
              </ul>
            </div>
          </div>
          
          </div>
        </div>
      </div>
      <div class="copyrights">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2017. All rights reserved.</p>
            </div>
            
          </div>
        </div>
      </div>
    </footer>
    
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/@fancyapps/fancybox/jquery.fancybox.min.js"></script>
    <script src="js/front.js"></script>
  </body>
</html>

<?php
}
else{
	echo "Session Reservée à l'administrateur.<br> <a href='loginAdmin'>Veuillez vous identifier pour accéder aux privilèges </a>";
}
?>