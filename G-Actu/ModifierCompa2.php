<?php
session_start();
 if(isset($_SESSION["autorisationAdmin"]))
			  {
require "ConnexionBDD.php";
$bdd=connect();
$lien1 = $_POST["Lien1"];
$lien2 = $_POST["Lien2"];
$numLien1 = $_SESSION["link1"];
$numLien2 = $_SESSION["link2"];

$sql1= "Update sitevente set lien = '$lien1' where numLien = $numLien1";
$resultat1 = $bdd -> exec ($sql1);
$sql2= "Update sitevente set lien = '$lien2' where numLien = $numLien2";
$resultat2 = $bdd -> exec ($sql2);

$numCompa=$_SESSION["numCompa"];

$sql3 = "Select nomJeu,nomConsole from comparateur natural join jeux 
natural join console 
where id=$numCompa";
$resultat3 = $bdd -> query($sql3);
$produit = $resultat3->fetch(PDO::FETCH_OBJ);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Gaming actu</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <!-- Fancybox-->
    <link rel="stylesheet" href="vendor/@fancyapps/fancybox/jquery.fancybox.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
	<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <header class="header">
      <!-- Main Navbar-->
      <nav class="navbar navbar-expand-lg fixed-top">
        <div class="search-area">
          <div class="search-area-inner d-flex align-items-center justify-content-center">
            <div class="close-btn"><i class="icon-close"></i></div>
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                <form action="#">
                  <div class="form-group">
                    <input type="search" name="search" id="search" placeholder="Qu'est ce que vous voulez rechercher ?" >
                    <button type="submit" class="submit"><i class="icon-search-1"></i></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          
          <div class="navbar-header d-flex align-items-center justify-content-between">
            <a href="index.php" class="navbar-brand"><img src="Image/logo.png" style="height:100px;"></a>
            
            <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span></span><span></span><span></span></button>
          </div>
          
          <div id="navbarcollapse" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a href="index.php" class="nav-link active ">Accueil</a>
              </li>
              <li class="nav-item"><a href="actualite.php" class="nav-link ">Actualité</a>
              </li>
              <li class="nav-item"><a href="comparateur.php" class="nav-link ">Comparateur</a>
              </li>
			 <li class="nav-item"><a href="Jeux.php" class="nav-link ">Jeux</a>
              </li>
			  			  <li class="nav-item"><a href="Snake.php" class="nav-link ">Snake</a>
              </li>
			  <li class="nav-item"><a href="admin.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexionAdmin.php" class="nav-link ">Se déconnecter</a>
              </li>
			   </ul>
            <div class="navbar-text">
			
			<form class="form-inline my-2 my-lg-0" action="recherche.php" method="POST">
				<input class="form-control mr-sm-2" type="search" name="produit" placeholder="Produit" aria-label="Produit">
				<button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="icon-search-1"></i> </button>
			</form>
            </div>
            
          </div>
        </div>
      </nav>
    </header>
    
    <section style=" margin-top:5%;background: url(Image/game.jpg); background-size: cover; background-position: center center" class="hero">
      <div class="container">
		</div>	  
    </section>
<form action="ModifierCompa3.php" method="POST"> 

<h1 style="text-align: center;"> Modifiez le(s) lien(s) du jeu choisi</h1>
	
		<div class="form-group">
		<label for="bonbon"> Nom du jeu (Avec le nom exact inscrit dans la BDD) :  
      <input type="text"  name="jeu"  value=" <?php echo $produit->nomJeu ?> " >
	</div>
    <br> </br>
		<div class="form-group">
		<label for="bonbon"> Nom de la console (Avec le nom exact inscrit dans la BDD):
      <input type="text" class="form-control" value="<?php echo $produit->nomConsole;?>"  name="console">
	  </div>
	  
		
    <br> </br>
		
 <button type="submit" class="btn btn-primary">Modifier</button>
 
</form>

<footer class="main-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo">
              
            </div>
            <div class="contact-details">
              <h3>Contact Staff</h3>
			  <div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/luffy.jpg" alt="..." class="img-fluid"></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">fatih-altintas.pro@outlook.fr</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/goku.jpg" alt="..." class="img-fluid" style=""></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">nassim.bougtib@hotmail.com</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/jojo.jpg" alt="..." class="img-fluid" style="height:150px;"></div>
                  <div class="title"><strong>Email:<br></strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">firask_92@hotmail.fr</a></span></div>
                </div>
            </div>
          </div>
          
          
        </div>
      </div>
	  <br>
    <br>
    
      <div class="copyrights">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2019. All rights reserved.</p>
            </div>
            
          </div>
        </div>
      </div>
    </footer>
    
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/@fancyapps/fancybox/jquery.fancybox.min.js"></script>
    <script src="js/front.js"></script>
  </body>
</html>

<?php

			  }else{
				  echo "Session Reservée à l'administrateur.<br> <a href='loginAdmin'>Veuillez vous identifier pour accéder aux privilèges </a>";
			  }
			  
			 ?>