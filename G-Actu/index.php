<?php
session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Gaming actu</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <!-- Fancybox-->
    <link rel="stylesheet" href="vendor/@fancyapps/fancybox/jquery.fancybox.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
	<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <header class="header">
      <!-- Main Navbar-->
      <nav class="navbar navbar-expand-lg fixed-top">
        <div class="search-area">
          <div class="search-area-inner d-flex align-items-center justify-content-center">
            <div class="close-btn"><i class="icon-close"></i></div>
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                <form action="#">
                  <div class="form-group">
                    <input type="search" name="search" id="search" placeholder="Qu'est ce que vous voulez rechercher ?" >
                    <button type="submit" class="submit"><i class="icon-search-1"></i></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          
          <div class="navbar-header d-flex align-items-center justify-content-between">
            <a href="index.php" class="navbar-brand"><img src="Image/logo.png" style="height:80px;"></a>
            
            <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span></span><span></span><span></span></button>
          </div>
          
          <div id="navbarcollapse" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a href="index.php" class="nav-link active ">Accueil</a>
              </li>
              <li class="nav-item"><a href="actualite.php" class="nav-link ">Actualité</a>
              </li>
              <li class="nav-item"><a href="comparateur.php" class="nav-link ">Comparateur</a>
              </li>
			 <li class="nav-item"><a href="Jeux.php" class="nav-link ">Jeux</a>
              </li>
			     			  <li class="nav-item"><a href="Snake.php" class="nav-link ">Snake</a>
              </li>
			  <?php 
 if(isset($_SESSION["autorisationUtil"]))
			  {
				   ?>
			<li class="nav-item"><a href="accueilUtil.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexion.php" class="nav-link ">Se déconnecter</a>
              </li>
			  <?php
			  }elseif(isset($_SESSION["autorisationAdmin"])&& !isset($_SESSION["autorisationUtil"])){
				  ?>
			<li class="nav-item"><a href="admin.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexionAdmin.php" class="nav-link ">Se déconnecter</a>
              </li>
			  <?php
			  }elseif(!isset($_SESSION["autorisationAdmin"])&& !isset($_SESSION["autorisationUtil"]) ){
				  
				  
				  
			  
				   ?>
			  <li class="nav-item"><a href="login.php" class="nav-link ">Membre</a>
              </li>
			  <li class="nav-item"><a href="loginAdmin.php" class="nav-link ">Admin</a>
              </li>
			 <?php
			  }
			  ?>
            </ul>
           <div class="navbar-text">
			
			<form class="form-inline my-2 my-lg-0" action="recherche.php" method="POST">
				<input class="form-control mr-sm-2" type="search" name="produit" placeholder="Produit" aria-label="Produit">
				<button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="icon-search-1"></i> </button>
			</form>
            </div>
            
          </div>
        </div>
      </nav>
    </header>
    
    <section style="margin-top:5%;background: url(Image/game.jpg); background-size: cover; background-position: center center;border-style:inset" class="hero">
      <div class="container">
		</div>	  
    </section>
    
    <section class="intro">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <h2 class="h3">Gaming actu</h2>
            <p class="text-big">Bienvenue sur notre site d'actualité jeux vidéos G-actu <br> Vous retrouverez ici les informations et nouveautés<br> des jeux du moments . Bonne lecture :) !</p>
          </div>
		  <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="img/mk11.jpg" class="d-block w-100" alt="...">
			</div>
			<div class="carousel-item">
				<img src="img/tw3.jpg" class="d-block w-100" alt="...">
			</div>
			<div class="carousel-item">
				<img src="img/rdr2.jpg" class="d-block w-100" alt="...">
			</div>
			</div>
			</div>
        </div>
      </div>
    </section>
    
    
    <section style="background: url(Image/ac.jpg); background-size: cover; background-position: center center;border-style:inset;" class="hero">
      <div class="container">
		</div>	  
    </section>

	<?php
	//connexion à la BDD via la fonction de connect de la page PdoBonbon
	require "ConnexionBDD.php";
	$bdd=connect();
	//requête
	$sql="select * from jeux where nomJeu LIKE (lower('Fifa 19')) ";
	$resultat = $bdd -> query($sql);
	$produit=$resultat->fetch(PDO::FETCH_OBJ)
	?>
	<div class="container" style="margin-left:25%;" >
	<div class="row">
	<div class="col md-4">
	<section class="actualité">
		<div class="container">
			<div class="row">
			
				<div class="jumbotron" style="background: url(img/texture.jpg); background-size: cover;border: ridge;margin-right:27.7%;box-shadow: 1px 1px 2px black, 0 0 25px blue, 0 0 5px darkblue;color: white;">
					<h1 class="display-4"style="color:white;">Dernière Actualités</h1>
					<p class="lead"style="color:white;"><?php echo $produit-> nomJeu;?></p>
					<img src="./<?php echo $produit->imageJeu?>"style="height:500px;width:800px;">
					<hr class="my-4">
					<p style="color:white;">Une nouvelle Mise à jour de fifa 19 vient de sortir</p>
					<a class="btn btn-primary btn-lg" href="ActuFifa.php" role="button"><i class="fab  fa-readme"> Lire l'article</i></a>
				</div>
			</div>
			</div>
		</div>
	</section>
	</div>
	
	<?php
	$sql2="select * from jeux where nomJeu LIKE (lower('Super Smash Bros Ultimate')) ";
	$resultat2 = $bdd -> query($sql2);
	$produit2=$resultat2->fetch(PDO::FETCH_OBJ)
	?>
	
	<section class="actualité"  >
		<div class="container">
			<div class="row">
				<div class="jumbotron" style="background: url(img/texture.jpg); background-size: cover;border: ridge;margin-right:27.7%;box-shadow: 1px 1px 2px black, 0 0 25px blue, 0 0 5px darkblue;color: white;">
					<h1 class="display-4" style="color:white;">Dernière Actualités</h1>
					<p class="lead" style="color:white;"><?php echo $produit2-> nomJeu;?></p>
					<img src="./<?php echo $produit2->imageJeu?>"style="height:500px;width:800px;">
					<hr class="my-4">
					<p style="color:white;">Une nouvelle Mise à jour de Smash Bros Ultimate !!</p>
					<a class="btn btn-primary btn-lg" href="ActuSmash.php" role="button"><i class="fab  fa-readme"> Lire l'article</i></a>
				</div>
				
			</div>
			
		</div>
	</section>
	</div>
	
    
	<section style="background: url(img/fif.jpg); background-size: cover; background-position: center center;border-style:inset" class="hero">
      <div class="container">
		</div>	  
    </section>
	<?php 
	$sqla= "select * from jeuxpopu natural join jeux";
	$resultata = $bdd -> query($sqla);
	
	$i=1;
	
	?>
    <section class="jsortie-moments">    
      <div class="container">
        <div class="row">
          <div class="col-md-6">
		  <h4 style="margin-left:170px;font-family: 'Anton', sans-serif;">JEUX POPULAIRES</h4>
           <ul class="list-group">
		   <?php
		   while($produita = $resultata->fetch(PDO::FETCH_OBJ))
{
		   
		   ?>
			<li class="list-group-item d-flex justify-content-between align-items-center">
			<?php
			if(isset($_SESSION["autorisationAdmin"])){
	?>
			<a href="SuppJeuPopu.php?numJeuPopu=<?php echo $produita-> numJeu; ?>" style="margin-left:-2%"><i class="fas fa-times" style="color:red"></i></a>
			<a href="ModifierJeuPopu.php?numJeuPopu=<?php echo $produita-> numJeu; ?>" style="margin-left:-10%"><i class="fas fa-edit"></i></a>
			<?php
}
?>
				<?php echo $produita -> nomJeu ?>   
			<span class="badge badge-warning badge-pill"> <?php echo $i ?></span>
			</li>
<?php
$i++;
}
$sqlb="select * from jeuxattendu join jeux on jeux.numJeu = jeuxattendu.numJeu";
$resultatb = $bdd -> query($sqlb);
$n=1;
?>			</ul>
<?php
if(isset($_SESSION["autorisationAdmin"])){
	?>
	<a href="AjouterJeuPopu.php?"><button type="button" class="btn btn-danger">Ajouter</button></a>
<?php
}
?>
			</div>
			<div class="col-md-6">
		  <h4 style="margin-left:170px;font-family: 'Anton', sans-serif;">JEUX LES PLUS ATTENDUS</h4>
           <ul class="list-group">
		   <?php
		   while($produitb = $resultatb->fetch(PDO::FETCH_OBJ))
{
		   
		   ?>
			<li class="list-group-item d-flex justify-content-between align-items-center">
			<?php
if(isset($_SESSION["autorisationAdmin"])){
	?>
			<a href="SuppJeuAttendu.php?numJeuAttendu=<?php echo $produitb-> numJeu; ?> "style="margin-left:-2%"><i class="fas fa-times" style="color:red"></i></a> 
			<a href="ModifierJeuAttendu.php?numJeuAttendu=<?php echo $produitb-> numJeu; ?> "style="margin-left:-10%"><i class="fas fa-edit"></i></a> <?php
}
?>
				<?php echo $produitb -> nomJeu ?>
			<span class="badge badge-danger badge-pill"> <?php echo $n ?></span>
			</li>
<?php
$n++;
}
?>			</ul>
<?php
if(isset($_SESSION["autorisationAdmin"])){
	?>
	<a href="AjouterJeuAttendu.php?"><button type="button" class="btn btn-danger">Ajouter</button></a>
<?php
}
?>
			</div>
          
      </div>
    </section>
    
    
    
   <footer class="main-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo">
              
            </div>
            <div class="contact-details">
              <h3>Contact Staff</h3>
			  <div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/luffy.jpg" alt="..." class="img-fluid"></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">fatih-altintas.pro@outlook.fr</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/goku.jpg" alt="..." class="img-fluid" style=""></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">nassim.bougtib@hotmail.com</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/jojo.jpg" alt="..." class="img-fluid" style="height:150px;"></div>
                  <div class="title"><strong>Email:<br></strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">firask_92@hotmail.fr</a></span></div>
                </div>
            </div>
          </div>
          
          
        </div>
      </div>
	  <br>
    <br>
    
      <div class="copyrights">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2019. All rights reserved.</p>
            </div>
            
          </div>
        </div>
      </div>
    </footer>
    
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/@fancyapps/fancybox/jquery.fancybox.min.js"></script>
    <script src="js/front.js"></script>
  </body>
</html>