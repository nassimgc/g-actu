<?php
session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Gaming actu</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <!-- Fancybox-->
    <link rel="stylesheet" href="vendor/@fancyapps/fancybox/jquery.fancybox.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <header class="header">
      <!-- Main Navbar-->
      <nav class="navbar navbar-expand-lg fixed-top">
        <div class="search-area">
          <div class="search-area-inner d-flex align-items-center justify-content-center">
            <div class="close-btn"><i class="icon-close"></i></div>
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                <form action="#">
                  <div class="form-group">
                    <input type="search" name="search" id="search" placeholder="Qu'est ce que vous voulez rechercher ?" >
                    <button type="submit" class="submit"><i class="icon-search-1"></i></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          
          <div class="navbar-header d-flex align-items-center justify-content-between">
            <a href="index.php" class="navbar-brand"><img src="Image/logo.png" style="height:100px;"></a>
            
            <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span></span><span></span><span></span></button>
          </div>
          
          <div id="navbarcollapse" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a href="index.php" class="nav-link active ">Accueil</a>
              </li>
              <li class="nav-item"><a href="actualite.php" class="nav-link ">Actualité</a>
              </li>
              <li class="nav-item"><a href="comparateur.php" class="nav-link ">Comparateur</a>
              </li>
			  <li class="nav-item"><a href="Jeux.php" class="nav-link ">Jeux</a>
              </li>
			  <li class="nav-item"><a href="Snake.php" class="nav-link ">Snake</a>
              </li>
			  <?php 
 if(isset($_SESSION["autorisationUtil"]))
			  {
				   ?>
			<li class="nav-item"><a href="accueilUtil.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexion.php" class="nav-link ">Se déconnecter</a>
              </li>
			  <?php
			  }elseif(isset($_SESSION["autorisationAdmin"])&& !isset($_SESSION["autorisationUtil"])){
				  ?>
			<li class="nav-item"><a href="admin.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexionAdmin.php" class="nav-link ">Se déconnecter</a>
              </li>
			  <?php
			  }elseif(!isset($_SESSION["autorisationAdmin"])&& !isset($_SESSION["autorisationUtil"]) ){
				  
				  
				  
			  
				   ?>
			  <li class="nav-item"><a href="login.php" class="nav-link ">Membre</a>
              </li>
			  <li class="nav-item"><a href="loginAdmin.php" class="nav-link ">Admin</a>
              </li>
			 <?php
			  }
			  ?>
            </ul>
           <div class="navbar-text">
			
			<form class="form-inline my-2 my-lg-0" action="recherche.php" method="POST">
				<input class="form-control mr-sm-2" type="search" name="produit" placeholder="Produit" aria-label="Produit">
				<button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="icon-search-1"></i> </button>
			</form>
            </div>
            
            
          </div>
        </div>
      </nav>
    </header>
    
    <section style="background: url(Image/game.jpg); background-size: cover; background-position: center center;border-style:inset" class="hero">
      <div class="container">
		</div>	  
    </section>
	<br>
	<?php
	require "ConnexionBDD.php";
$bdd=connect();
//requête
$sql="select * from jeux where nomJeu LIKE (lower('Dragon Ball FighterZ')) ";
$resultat = $bdd -> query($sql);
$produit=$resultat->fetch(PDO::FETCH_OBJ)
	?>
	

	
<div class="card text-white bg-dark mb-3" style="width: 20rem;border: solid;background: url(img/texture.jpg);background-size: cover;box-shadow: 1px 1px 2px black, 0 0 25px pink, 0 0 5px pink;color: white;">
 <img src="./<?php echo $produit->imageJeu?>"class="card-img-top" alt="..." style="height:300px;">
  <div class="card-body">
    
    <p>Nom : <?php echo $produit-> nomJeu; ?><br>
				Age :<img src="./<?php echo $produit -> age;?>"style="height:60px;width:60px;"> <br>
				Description : <?php echo $produit -> description ?> <i class="fas fa-3x fa-fist-raised"></i>;
			</p>
  </div>
</div>
	
	
	<hr class="my-4" style="background-color:black;">
<i class="far  fa-4x fa-newspaper" style="padding-left:47%;"></i>

<h1 style="text-align: center;">Goku [GT] sera disponible le 9 mai dans Dragon Ball FighterZ</h1>
<br />
<p style="text-align: center;">Le protagoniste principal de Dragon Ball sera bientôt décliné pour la cinquième fois dans Dragon Ball FighterZ avec la sortie de Goku [GT] qui aura lieu le 9 mai, annonce Bandai Namco.</p>
</ br>
</ br>
<p style="text-align: center;">Ramené à l'état d'enfant par un voeu de Pilaf dans Dragon Ball GT, ce personnage est capable de déclencher un<br> Super Kamehameha qui le transforme en SSJ3 ainsi qu'une attaque ultime pendant laquelle il adopte une forme<br> SSJ4. Le Super Genkidama, le Bâton Magique et le Kamehameha Inversé font également partie de la panoplie<br> de ce combattant rapide et technique. Inclut avec cinq autres personnages dans le Fighter Pass 2 (24,99 euros)<br>, Goku [GT] sera aussi disponible séparément au prix de 4,99 euros.

</p>

<hr class="my-4">
<h1 style="text-align: center;">Vidéo de présentation</h1>
<br>
<i class="fas  fa-3x fa-video" style="margin-left:48%;"></i>
<br>
<br>
<div class="embed-responsive embed-responsive-16by9" style="width:50%;margin-left:480px;"">
  <iframe width="1280" height="720" src="https://www.youtube.com/embed/p4BvekYjs-c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<br>
<br>
<hr class="my-4" style="background-color:black;">


<br>
<section style="background: url(Image/dbz.jpg); background-size: cover; background-position: center center;border-style:inset" class="hero">
      <div class="container">
		</div>	  
    </section>
<br>
<br>



	<footer class="main-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo">
              
            </div>
            <div class="contact-details">
              <h3>Contact Staff</h3>
			  <div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/luffy.jpg" alt="..." class="img-fluid"></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">fatih-altintas.pro@outlook.fr</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/goku.jpg" alt="..." class="img-fluid" style=""></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">nassim.bougtib@hotmail.com</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/jojo.jpg" alt="..." class="img-fluid" style="height:150px;"></div>
                  <div class="title"><strong>Email:<br></strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">firask_92@hotmail.fr</a></span></div>
                </div>
            </div>
          </div>
          
          
        </div>
      </div>
	  <br>
    <br>
    
      <div class="copyrights">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2019. All rights reserved.</p>
            </div>
            
          </div>
        </div>
      </div>
    </footer>
    
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/@fancyapps/fancybox/jquery.fancybox.min.js"></script>
    <script src="js/front.js"></script>
  </body>
</html>