<?php
session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Gaming actu</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Fancybox-->
    <link rel="stylesheet" href="vendor/@fancyapps/fancybox/jquery.fancybox.min.css">
	<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body style="background: url(Image/back2.jpg); background-size: cover; background-position: center center;">
    <header class="header">
      <!-- Main Navbar-->
      <nav class="navbar navbar-expand-lg fixed-top">
        <div class="search-area">
          <div class="search-area-inner d-flex align-items-center justify-content-center ">
            <div class="close-btn"><i class="icon-close"></i></div>
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                <form action="#">
                  <div class="form-group">
                    <input type="search" name="search" id="search" placeholder="Qu'est ce que vous voulez rechercher ?" >
                    <button type="submit" class="submit"><i class="icon-search-1"></i></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          
          <div class="navbar-header d-flex align-items-center justify-content-between">
            <a href="index.php" class="navbar-brand"><img src="Image/logo.png" style="height:100px;"></a>
            
            <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span></span><span></span><span></span></button>
          </div>
          
          <div id="navbarcollapse" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a href="index.php" class="nav-link active ">Accueil</a>
              </li>
              <li class="nav-item"><a href="actualite.php" class="nav-link ">Actualité</a>
              </li>
              <li class="nav-item"><a href="comparateur.php" class="nav-link ">Comparateur</a>
              </li>
			  <li class="nav-item"><a href="Jeux.php" class="nav-link ">Jeux</a>
              </li>
			  			  <li class="nav-item"><a href="Snake.php" class="nav-link ">Snake</a>
              </li>
<?php 
			  if(isset($_SESSION["autorisationUtil"]))
			  {
				   ?>
			<li class="nav-item"><a href="accueilUtil.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexion.php" class="nav-link ">Se déconnecter</a>
              </li>
			  <?php
			  }elseif(isset($_SESSION["autorisationAdmin"])&& !isset($_SESSION["autorisationUtil"])){
				  ?>
			<li class="nav-item"><a href="admin.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexionAdmin.php" class="nav-link ">Se déconnecter</a>
              </li>
			  <?php
			  }elseif(!isset($_SESSION["autorisationAdmin"])&& !isset($_SESSION["autorisationUtil"]) ){
				  
				  
				  
			  
				   ?>
			  <li class="nav-item"><a href="login.php" class="nav-link ">Membre</a>
              </li>
			  <li class="nav-item"><a href="loginAdmin.php" class="nav-link ">Admin</a>
              </li>
			 <?php
			  }
			  ?>
            </ul>
           <div class="navbar-text">
			
			<form class="form-inline my-2 my-lg-0" action="recherche.php" method="POST">
				<input class="form-control mr-sm-2" type="search" name="produit" placeholder="Produit" aria-label="Produit">
				<button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="icon-search-1"></i> </button>
			</form>
            </div>
            
          </div>
        </div>
      </nav>
    </header>
    
    <section style="margin-top:5%;background: url(Image/game.jpg); background-size: cover; background-position: center center;border-style:inset;" class="hero">
      <div class="container">
		</div>	  
		<?php
		if(isset($_SESSION["autorisationAdmin"])){
			?>
		<a href="AjouterCompa.php"><button type="button" class="btn btn-danger">Ajouter</button></a>
		<?php
		}
		?>
    </section>
	<br><br>
	<i class="far  fa-4x fa-money-bill-alt" style="margin-left:49%;color:white;"></i>
	<h1 style="text-align: center;color:white; text-decoration:underline;"> Bienvenue sur notre page Comparateur</h1>
	
	<p style="text-align: center;color:white;">Vous pourrez trouver vos jeux au meilleur prix selon la plateforme de votre choix</p>
	<br>
	<?php
	require "ConnexionBDD.php";
$bdd=connect();
//requête
$sql="select imageCompa,id, nomJeu,numLien1,numLien2, s1.lien as 'cc',s2.lien as 'ss', nomConsole from comparateur 
INNER JOIN sitevente AS s1
ON comparateur.numLien1 = s1.numLien
INNER JOIN sitevente AS s2
ON comparateur.numLien2 = s2.numLien
join jeux on jeux.numJeu = comparateur.numJeu 
join Console on console.numConsole= comparateur.numConsole
Group by comparateur.numJeu,comparateur.numConsole
Order By nomConsole";
$resultat = $bdd -> query($sql);
	
	while($produit = $resultat->fetch(PDO::FETCH_OBJ))
{
		//echo "<tr> <td>".$produit->nom."</td> <td>".$produit->prix." €</td> <td> <img src='".$produit->photo."'></td></tr>";
		
?>
<div class="card text-white bg-dark mb-3" style="max-width: 18rem; display:inline-block;margin-left:6%;width:245px;border: solid;background: url(img/texture.jpg);background-size: cover;box-shadow: 1px 1px 2px black, 0 0 25px pink, 0 0 5px pink;color: white;">
  <img id="img" src=".<?php echo $produit-> imageCompa?>"class="card-img-top" alt="..." style="height:300px;">
  <br><br>
  <div class="card-body">
    <p class="card-text"> <?php echo $produit-> nomJeu ?> </p>
	<p class="card-text"><?php echo $produit->  nomConsole?>  </p>
	<a href="<?php echo $produit->cc ?>" style="font-size:20px;color:white;"> Lien amazon  </a> <i class="fab fa-1x fa-amazon"></i> <br>
	<a href="<?php echo $produit->ss ?>" style="font-size:20px;color:white;"> Lien fnac </a>  <img src="Image/fnac.png" style="height:35px;">
  <?php
	if(isset($_SESSION["autorisationAdmin"])){
		?>
		
	<a href="ModifierCompa.php?choix=<?php echo $produit-> id;?>"><button type="button" class="btn btn-danger" style="display:inline-block">Modifier</button></a> <hr>
	<a href="SupprimerCompa.php?choix=<?php echo $produit->id;?>"><button type="button" class="btn btn-danger">Supprimer</button></a>
		
		<?php
	}
	?>
  
  </div>

</div>

<?php
}
?>

<br>
<br>
	<footer class="main-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo">
              
            </div>
            <div class="contact-details">
              <h3>Contact Staff</h3>
			  <div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/luffy.jpg" alt="..." class="img-fluid"></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">fatih-altintas.pro@outlook.fr</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/goku.jpg" alt="..." class="img-fluid" style=""></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">nassim.bougtib@hotmail.com</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/jojo.jpg" alt="..." class="img-fluid" style="height:150px;"></div>
                  <div class="title"><strong>Email:<br></strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">firask_92@hotmail.fr</a></span></div>
                </div>
            </div>
          </div>
          
          
        </div>
      </div>
	  <br>
    <br>
    
      <div class="copyrights">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2019. All rights reserved.</p>
            </div>
            
          </div>
        </div>
      </div>
    </footer>
    
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/@fancyapps/fancybox/jquery.fancybox.min.js"></script>
    <script src="js/front.js"></script>
  </body>
</html>