<!DOCTYPE html">
<html lang="fr">
 <head>
<link href="lga.css" rel="stylesheet">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <!-- Fancybox-->
    <link rel="stylesheet" href="vendor/@fancyapps/fancybox/jquery.fancybox.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
</head>
<body>
<header class="header">
      <!-- Main Navbar-->
      <nav class="navbar navbar-expand-lg">
        <div class="search-area">
          <div class="search-area-inner d-flex align-items-center justify-content-center">
            <div class="close-btn"><i class="icon-close"></i></div>
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          
          <div class="navbar-header d-flex align-items-center justify-content-between">
            <a href="index.php" class="navbar-brand"><img src="Image/logo.png" style="height:100px;"></a>
            
            <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span></span><span></span><span></span></button>
          </div>
          
          <div id="navbarcollapse" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a href="index.php" class="nav-link active ">Accueil</a>
              </li>
              <li class="nav-item"><a href="actualite.php" class="nav-link ">Actualité</a>
              </li>
              <li class="nav-item"><a href="comparateur.php" class="nav-link ">Comparateur</a>
              </li>
			  <li class="nav-item"><a href="Jeux.php" class="nav-link ">Jeux</a>
              </li>
			  			  <li class="nav-item"><a href="Snake.php" class="nav-link ">Snake</a>
              </li>
			  <li class="nav-item"><a href="login.php" class="nav-link ">Membre</a>
              </li>
            </ul>
			<div class="navbar-text">
			
			<form class="form-inline my-2 my-lg-0" action="recherche.php" method="POST">
				<input class="form-control mr-sm-2" type="search" name="produit" placeholder="Produit" aria-label="Produit">
				<button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="icon-search-1"></i> </button>
			</form>
            </div>
            
            
          </div>
        </div>
      </nav>
    </header>
	

    <div id="login" style="margin-top:5%;">
    
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-9">
                        <form id="login-form" class="form" action="verif.php" method="post">
                            <h3 class="text-center text-info">Connexion</h3>
                            <div class="form-group">
                                <label for="username" class="text-info">Pseudo:</label><br>
                                <input type="text" name="identi" id="pseudo" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Mot de Passe:</label><br>
                                <input type="password" name="mdp" id="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="Se connecter">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<br>
	
	
	
	
	<footer class="main-footer" style="margin-top:5%;">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo">
              
            </div>
            <div class="contact-details">
              <h3>Contact Staff</h3>
			  <div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/luffy.jpg" alt="..." class="img-fluid"></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">fatih-altintas.pro@outlook.fr</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/goku.jpg" alt="..." class="img-fluid" style=""></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">nassim.bougtib@hotmail.com</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/jojo.jpg" alt="..." class="img-fluid" style="height:150px;"></div>
                  <div class="title"><strong>Email:<br></strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">firask_92@hotmail.fr</a></span></div>
                </div>
            </div>
          </div>
          
          
        </div>
      </div>
	  <br>
    <br>
    
      <div class="copyrights">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2019. All rights reserved.</p>
            </div>
            
          </div>
        </div>
      </div>
    </footer>
    
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/@fancyapps/fancybox/jquery.fancybox.min.js"></script>
    <script src="js/front.js"></script>
  </body>
</html>