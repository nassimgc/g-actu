<?php
session_start();
if(isset($_SESSION["autorisationUtil"])){
$id=$_GET["choix"];
$_SESSION["numJeu"]=$id;
$pseudo= $_SESSION["identifiant"];
require "ConnexionBDD.php";
$bdd=connect();
$sql="select * from Jeux where numJeu = $id";
$resultat = $bdd -> query($sql);
$produit = $resultat->fetch(PDO::FETCH_OBJ)
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Gaming actu</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Fancybox-->
    <link rel="stylesheet" href="vendor/@fancyapps/fancybox/jquery.fancybox.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body style="background: url(Image/back2.jpg); background-size: cover; background-position: center center;">
    <header class="header">
      <!-- Main Navbar-->
      <nav class="navbar navbar-expand-lg fixed-top ">
        <div class="search-area">
          <div class="search-area-inner d-flex align-items-center justify-content-center ">
            <div class="close-btn"><i class="icon-close"></i></div>
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                <form action="#">
                  <div class="form-group">
                    <input type="search" name="search" id="search" placeholder="Qu'est ce que vous voulez rechercher ?" >
                    <button type="submit" class="submit"><i class="icon-search-1"></i></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          
          <div class="navbar-header d-flex align-items-center justify-content-between">
            <a href="index.php" class="navbar-brand"><img src="Image/logo.png" style="height:100px;"></a>
            
            <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span></span><span></span><span></span></button>
          </div>
          
          <div id="navbarcollapse" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a href="index.php" class="nav-link active ">Accueil</a>
              </li>
              <li class="nav-item"><a href="actualite.php" class="nav-link ">Actualité</a>
              </li>
              <li class="nav-item"><a href="comparateur.php" class="nav-link ">Comparateur</a>
              </li>
			  <li class="nav-item"><a href="Jeux.php" class="nav-link ">Jeux</a>
              </li>
			   <?php 
			  if(isset($_SESSION["autorisationUtil"]))
			  {
				   ?>
			<li class="nav-item"><a href="accueilUtil.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexion.php" class="nav-link ">Se déconnecter</a>
              </li>
			  <?php
			  }else{
				  ?>
			<li class="nav-item"><a href="login.php" class="nav-link ">Membre</a>
              </li>
			  <?php
			  }
			  ?>
            </ul>
           <div class="navbar-text">
			
			<form class="form-inline my-2 my-lg-0" action="recherche.php" method="POST">
				<input class="form-control mr-sm-2" type="search" name="produit" placeholder="Produit" aria-label="Produit">
				<button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="icon-search-1"></i> </button>
			</form>
            </div>
            
          </div>
        </div>
      </nav>
    </header>
    
    <section style="margin-top:6%;background: url(Image/game.jpg); background-size: cover; background-position: center center" class="hero">
      <div class="container">
		</div>	  
    </section>
	<br>
	
	<div class="card text-white bg-dark mb-3" style="max-width: 18rem; display:inline-block; margin-left:40%;border: solid;background: url(img/text-2.jpg);background-size: cover;box-shadow: 1px 1px 2px black, 0 0 25px pink, 0 0 5px pink;color: white;">
  <img src="<?php echo $produit-> imageJeu ?>" class="card-img-top" alt="..." style="width:250px ;  height:300px;">
  <div class="card-body">
    <p class="card-text"> <?php echo $produit-> nomJeu ?> </p>
	<img src=".<?php echo $produit-> age ?>" style="width:60px; height:60px;">
	<p class="card-text"> <?php echo $produit-> description ?> </p>
	</div>
	</div>
	
	<?php
	$sql2="select pseudo,commentaire,numAvis from avisUtil where numJeu = $id ";
	$resultat2 = $bdd -> query($sql2);
	while($produit2 = $resultat2->fetch(PDO::FETCH_OBJ))
{
	
	?>
	<section class="comm" style="border:solid;gray;">
	<?php 
	if(isset($_SESSION["autorisationAdmin"])){
		
		?>
	<a href="SuppComment.php?Comment="<?php echo $produit2-> numAvis ?>><i class="fas fa-times"></i> </a>
	<?php 
	}
	?>
	<div class="com" style="background: url(img/text-2.jpg);background-size: cover; background-position: center center;margin-left:20%;margin-right:20%;margin-top:3%; border:solid;box-shadow: 1px 1px 2px black, 0 0 25px pink, 0 0 5px pink;color: white;">
	<p style="font-size:25px;margin-left:5%;margin-top:3%;font-family: 'Open Sans Condensed', sans-serif;"><i class="fas fa-2x fa-user-secret"style="margin-top:3%;"></i> Ecrit par : <?php echo $produit2-> pseudo ?> </p>
	<hr class="my-4">
	<div class="ms" style="display:inline-block;margin-left:5%">
	<iframe src="https://giphy.com/embed/S4GyRtqOcoCVOWlxwV" width="50" height="50" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
	<p style="font-family: 'Open Sans Condensed', sans-serif;font-size:25px;"> <?php echo $produit2-> commentaire ?> </p>
	</div>
	</div>
	<?php
}
	?>
	<br>
	<br>
	
	
	<form action="CommentUtil.php" method="POST" style="margin-left:20%;margin-right:20%;">
<div class="form-group">
    <label for="exampleFormControlTextarea1" style="color:white;">Poster un commentaire : <i class="fas fa-2x fa-pencil-alt"></i> </label>
    <textarea class="form-control" name="comment" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
<button type="submit" class="btn btn-dark">Poster</button>
</form>
</section>

<footer class="main-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo">
              
            </div>
            <div class="contact-details">
              <h3>Contact Staff</h3>
			  <div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/luffy.jpg" alt="..." class="img-fluid"></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">fatih-altintas.pro@outlook.fr</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/goku.jpg" alt="..." class="img-fluid" style=""></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">nassim.bougtib@hotmail.com</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/jojo.jpg" alt="..." class="img-fluid" style="height:150px;"></div>
                  <div class="title"><strong>Email:<br></strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">firask_92@hotmail.fr</a></span></div>
                </div>
            </div>
          </div>
          
          
        </div>
      </div>
	  <br>
    <br>
    
      <div class="copyrights">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2019. All rights reserved.</p>
            </div>
            
          </div>
        </div>
      </div>
    </footer>
    
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/@fancyapps/fancybox/jquery.fancybox.min.js"></script>
    <script src="js/front.js"></script>
  </body>
</html>
<?php
}
else{
$id=$_GET["choix"];
require "ConnexionBDD.php";
$bdd=connect();
$sql="select * from Jeux where numJeu = $id";
$resultat = $bdd -> query($sql);
$produit = $resultat->fetch(PDO::FETCH_OBJ)
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Gaming actu</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Fancybox-->
    <link rel="stylesheet" href="vendor/@fancyapps/fancybox/jquery.fancybox.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body style="background: url(Image/back2.jpg); background-size: cover; background-position: center center;">
    <header class="header">
      <!-- Main Navbar-->
      <nav class="navbar navbar-expand-lg">
        <div class="search-area">
          <div class="search-area-inner d-flex align-items-center justify-content-center">
            <div class="close-btn"><i class="icon-close"></i></div>
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                <form action="#">
                  <div class="form-group">
                    <input type="search" name="search" id="search" placeholder="Qu'est ce que vous voulez rechercher ?" >
                    <button type="submit" class="submit"><i class="icon-search-1"></i></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          
          <div class="navbar-header d-flex align-items-center justify-content-between">
            <a href="index.php" class="navbar-brand"><img src="Image/logo.png" style="height:150px;"></a>
            
            <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span></span><span></span><span></span></button>
          </div>
          
           <div id="navbarcollapse" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a href="index.php" class="nav-link active ">Accueil</a>
              </li>
              <li class="nav-item"><a href="actualite.php" class="nav-link ">Actualité</a>
              </li>
              <li class="nav-item"><a href="comparateur.php" class="nav-link ">Comparateur</a>
              </li>
			 <li class="nav-item"><a href="Jeux.php" class="nav-link ">Jeux</a>
              </li>
			   <?php 
 if(isset($_SESSION["autorisationUtil"]))
			  {
				   ?>
			<li class="nav-item"><a href="accueilUtil.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexion.php" class="nav-link ">Se déconnecter</a>
              </li>
			  <?php
			  }elseif(isset($_SESSION["autorisationAdmin"])&& !isset($_SESSION["autorisationUtil"])){
				  ?>
			<li class="nav-item"><a href="admin.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexionAdmin.php" class="nav-link ">Se déconnecter</a>
              </li>
			  <?php
			  }elseif(!isset($_SESSION["autorisationAdmin"])&& !isset($_SESSION["autorisationUtil"]) ){
				  
				  
				  
			  
				   ?>
			  <li class="nav-item"><a href="login.php" class="nav-link ">Membre</a>
              </li>
			  <li class="nav-item"><a href="loginAdmin.php" class="nav-link ">Admin</a>
              </li>
			 <?php
			  }
			  ?>
            </ul>
          <form class="form-inline my-2 my-lg-0" action="recherche.php" method="POST">
				<input class="form-control mr-sm-2" type="search" name="produit" placeholder="Produit" aria-label="Produit">
				<button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="icon-search-1"></i> </button>
			</form>
            
          </div>
        </div>
      </nav>
    </header>
    
    <section style="background: url(Image/game.jpg); background-size: cover; background-position: center center" class="hero">
      <div class="container">
		</div>	  
    </section>
	
	<div class="card text-white bg-dark mb-3" style="margin-top:3%;max-width: 18rem; display:inline-block; margin-left:40%;border: solid;background: url(img/text-2.jpg);background-size: cover;box-shadow: 1px 1px 2px black, 0 0 25px pink, 0 0 5px pink;color: white;">
  <img src="<?php echo $produit-> imageJeu ?>" class="card-img-top" alt="..." style="width:250px ;  height:300px;">
  <div class="card-body">
    <p class="card-text"> <?php echo $produit-> nomJeu ?> </p>
	<img src=".<?php echo $produit-> age ?>" style="width:60px; height:60px;">
	<p class="card-text"> <?php echo $produit-> description ?> </p>
	</div>
	</div>
	
	<h1 style="margin-left:35%;color:white;">Tous les commentaires</h1>
	
	<?php
	$sql2="select pseudo,commentaire, numAvis from avisUtil where numJeu = $id";
	$resultat2 = $bdd -> query($sql2);
	while($produit2 = $resultat2->fetch(PDO::FETCH_OBJ))
{
	
	?>
	<section class="comm">

	<div class="com" style="background: url(img/text-2.jpg);background-size: cover; background-position: center center;margin-left:20%;margin-right:20%;margin-top:3%; border:solid;box-shadow: 1px 1px 2px black, 0 0 25px pink, 0 0 5px pink;color: white;">
		<?php 
	if(isset($_SESSION["autorisationAdmin"])){
		
		?>
	<a href="SuppComment.php?numComment=<?php echo $produit2-> numAvis; ?> "style="margin-left:97%"><i class="fas fa-2x fa-times" style="color:red"></i> </a>
	<?php 
	}
	?>
	<p style="font-size:25px;margin-left:5%;margin-top:3%;font-family: 'Open Sans Condensed', sans-serif;"><i class="fas fa-2x fa-user-secret"style="margin-top:3%;"></i> Ecrit par : <?php echo $produit2-> pseudo ?> </p>
	<hr class="my-4">
	<p style="font-family: 'Open Sans Condensed', sans-serif;font-size:25px;margin-left:5%;"> <?php echo $produit2-> commentaire ?> </p>
	</div>
	</section>
	
	<?php
}
	?>
	<br>
	<br>
	<br>
	<?php
	if(isset($_SESSION["autorisationAdmin"]) && !isset($_SESSION["autorisationUtil"])){
		
	}else{
		?>
<a href="login.php" style="margin-left:35%;margin-bottom:3%;"> Vous devez être connectés pour poster un commentaire </a>
<?php
	}
	?>
   <footer class="main-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo">
              
            </div>
            <div class="contact-details">
              <h3>Contact Staff</h3>
			  <div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/luffy.jpg" alt="..." class="img-fluid"></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">fatih-altintas.pro@outlook.fr</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/goku.jpg" alt="..." class="img-fluid" style=""></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">nassim.bougtib@hotmail.com</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/jojo.jpg" alt="..." class="img-fluid" style="height:150px;"></div>
                  <div class="title"><strong>Email:<br></strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">firask_92@hotmail.fr</a></span></div>
                </div>
            </div>
          </div>
          
          
        </div>
      </div>
	  <br>
    <br>
    
      <div class="copyrights">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2019. All rights reserved.</p>
            </div>
            
          </div>
        </div>
      </div>
    </footer>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/@fancyapps/fancybox/jquery.fancybox.min.js"></script>
    <script src="js/front.js"></script>
  </body>
</html>
<?php
}
?>